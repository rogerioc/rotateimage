package com.example.screentest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class Screen extends View {
	private Context mContext;
	private Bitmap mSpaceShip;
	Point point;
	double rotate;
	private float centerX;
	private float centerY;
	public Screen(Context context) {
		super(context);
		mContext = context;
		mSpaceShip = getImage(R.drawable.nav);
		point = new Point();
		centerX = mSpaceShip.getWidth()/2;
		centerY = mSpaceShip.getHeight()/2;
		
	}
	private Bitmap getImage(int id){
		return BitmapFactory.decodeResource(mContext.getResources(),id);
	}
	@Override
	public void draw(Canvas canvas) {	
		super.draw(canvas);
		
		//float rotate=(float)Math.atan2(posicaoX,posicaoY);        		
		Matrix matrix = new Matrix();
		//float r=3.141516f / 2 ;
		//float r=(float)Math.atan2(point.x,point.y) ;//+ 3.141516f ;
		//matrix.setTranslate(mSpaceShip.getWidth()/2, mSpaceShip.getHeight()/2);
		matrix.preRotate((float)Math.toDegrees(rotate), mSpaceShip.getWidth()/2, mSpaceShip.getHeight()/2);
		//matrix.postRotate((float)Math.toDegrees(r));
		Bitmap resizedBitmap = Bitmap.createBitmap(mSpaceShip,0,0 ,
				mSpaceShip.getWidth(), mSpaceShip.getHeight(), matrix, true);
		canvas.drawBitmap(resizedBitmap, 10,10,null);
		
	}	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		 float currentX = event.getX();
		 float currentY = event.getY();
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_MOVE:
            Log.d(VIEW_LOG_TAG, "Pos" + point.x + " "+ point.y);
            rotate = Math.atan2(currentX - centerX,centerY - currentY);
            //dialer.rotationAngle = (int) Math.toDegrees(rotationAngleRadians);            
            break;
		}
		
		this.invalidate();
		//return super.onTouchEvent(event);
		return true;
	}
	
	 protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	        super.onSizeChanged(w, h, oldw, oldh);	   
	 }
	
	/*@Override
	public boolean onTouch(View v, MotionEvent event) {
		point.x = (int) event.getRawX();
		point.y = (int) event.getRawY();
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_MOVE:
            Log.d(VIEW_LOG_TAG, "Pos" + point.x + " "+ point.y);
            break;
		}
		
		this.invalidate();
		return false;
	}*/
}
